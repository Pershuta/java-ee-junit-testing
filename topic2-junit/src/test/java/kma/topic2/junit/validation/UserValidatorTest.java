package kma.topic2.junit.validation;

import kma.topic2.junit.exceptions.ConstraintViolationException;
import kma.topic2.junit.exceptions.LoginExistsException;
import kma.topic2.junit.model.NewUser;
import kma.topic2.junit.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserValidatorTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserValidator userValidator;

    @Test
    void shouldPassValidation() {
        userValidator.validateNewUser(
                NewUser.builder()
                        .login("log")
                        .password("pas")
                        .fullName("new name")
                        .build());

        verify(userRepository).isLoginExists("log");
    }

    @Test
    void shouldThrowExceptionIfLoginExists() {
        Mockito.when(userRepository.isLoginExists("log")).thenReturn(true);
        assertThatThrownBy(() -> userValidator.validateNewUser(NewUser.builder()
                .login("log")
                .password("pas")
                .fullName("new name")
                .build()))
                .isInstanceOf(LoginExistsException.class);
    }

    @ParameterizedTest
    @MethodSource("passwordData")
    void shouldThrowExceptionIfPasswordLengthIsLessThen3(String password, List<String> errors) {
        assertThatThrownBy(() -> userValidator.validateNewUser(NewUser.builder()
                .login("log")
                .password(password)
                .fullName("new name")
                .build()))
                .isInstanceOfSatisfying(
                        ConstraintViolationException.class,
                        ex -> assertEquals(ex.getErrors(),errors));
    }
    private static Stream<Arguments> passwordData(){
        return Stream.of(
                Arguments.of("pa",Arrays.asList("Password has invalid size")),
                Arguments.of("password",Arrays.asList("Password has invalid size")),
                Arguments.of("pa.",Arrays.asList("Password doesn't match regex")),
                Arguments.of(".", Arrays.asList("Password has invalid size","Password doesn't match regex"))
        );
    }
}