package kma.topic2.junit.service;

import kma.topic2.junit.model.NewUser;
import kma.topic2.junit.model.User;
import kma.topic2.junit.repository.UserRepository;
import kma.topic2.junit.validation.UserValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @SpyBean
    private UserValidator userValidator;

    @Test
    void shouldCreateUser(){
        userService.createNewUser(
                NewUser.builder()
                        .login("log")
                        .password("pas")
                        .fullName("new name")
                        .build()
        );

        verify(userValidator).validateNewUser(
                NewUser.builder()
                        .login("log")
                        .password("pas")
                        .fullName("new name")
                        .build()
        );

        assertThat(userRepository.getUserByLogin("log"))
                .returns("pas", User::getPassword)
                .returns("new name", User::getFullName);
    }

    @Test
    void shouldFindUserUserByLogin(){
        assertThat(userService.getUserByLogin("log"))
                .returns("pas", User::getPassword)
                .returns("new name", User::getFullName);
    }
}