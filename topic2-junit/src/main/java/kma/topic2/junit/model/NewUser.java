package kma.topic2.junit.model;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Data
@RequiredArgsConstructor
@Builder
public class NewUser {

    private final String login;
    private final String password;
    private final String fullName;

}
